const { authJwt} = require("../middlewares");
const controller = require("../controllers/user.controller");

module.exports = function(app) {
  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  // get user data by id route
  app.get(
    "/api/users/:id", 
    [
      authJwt.verifyToken,
      authJwt.isUser,
    ], 
    controller.getDataUser
  );

  // update user data by id route
  app.patch(
    "/api/users/:id", 
    [
      authJwt.verifyToken,
      authJwt.isUser,
    ], 
    controller.updateDataUser);

};