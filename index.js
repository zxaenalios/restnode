const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cors = require("cors");
const dbConfig = require("./src/config/db.config");
const bcrypt = require("bcryptjs");

// cors
const corsOptions = {
    origin: "http://localhost:3000"
};
app.use(cors(corsOptions));

// parse requests of content-type - application/json
app.use(bodyParser.json());

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// import db models

const db = require("./src/models");
const Role = db.role;
const User = db.user;

// connect with mongodb
db.mongoose
  .connect(`mongodb://${dbConfig.HOST}:${dbConfig.PORT}/${dbConfig.DB}`, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  })
  .then(() => {
    console.log("Successfully connect to MongoDB.");
    initial();
  })
  .catch(err => {
    console.error("Connection error", err);
    process.exit();
  });

// routes
require("./src/routes/admin.routes")(app);
require("./src/routes/auth.routes")(app);
require("./src/routes/user.routes")(app);

// set port, listen for requests
const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});

// init create db collections roles (user, admin) & users (admin)
function initial() {
  Role.estimatedDocumentCount((err, count) => {
    if (!err && count === 0) {
      new Role({
        name: "user"
      }).save(err => {
        if (err) {
          console.log("error", err);
        }
        console.log("Add 'user' to roles collection");
      });
      new Role({
        name: "admin"
      }).save(err => {
        if (err) {
          console.log("error", err);
        }
        console.log("Add 'user' to roles collection");
      });
    }
  });

  User.estimatedDocumentCount((err, count) => {
    if (!err && count === 0) {
      Role.findOne({ name: "admin" }, (err, role) => {
        if (err) {
          res.status(500).send({ message: err });
          return;
        }
        new User({
          username: "admin",
          password: bcrypt.hashSync("admin123", 8),
          roles: [role._id]
        }).save(err => {
          if (err) {
            console.log("error", err);
          }
          console.log("add account username 'admin' with password 'admin123'");
        });
      });
    }
  });
}
